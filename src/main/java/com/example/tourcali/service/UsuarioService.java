package com.example.tourcali.service;

import com.example.tourcali.DTO.UsuarioDto;
import com.example.tourcali.model.Usuario;

import java.security.NoSuchAlgorithmException;
import java.util.List;

public interface UsuarioService {

    Usuario loguear(UsuarioDto usuarioDto) throws NoSuchAlgorithmException;

    Usuario cambiarClave(UsuarioDto usuarioDto) throws NoSuchAlgorithmException;

    String registrar(UsuarioDto usuarioDto);

    String actualizar(UsuarioDto usuarioDto);

    String eliminar(Long id);

    Usuario consultarUsuarioById(Long id);

    List<Usuario> consultarUsuarios();

}
