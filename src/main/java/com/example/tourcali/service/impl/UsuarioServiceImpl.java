package com.example.tourcali.service.impl;

import com.example.tourcali.DTO.UsuarioDto;
import com.example.tourcali.model.Usuario;
import com.example.tourcali.repository.UsuarioRepository;
import com.example.tourcali.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.NoSuchAlgorithmException;
import java.util.List;

@Service
public class UsuarioServiceImpl implements UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Override
    public Usuario loguear(UsuarioDto usuarioDto) throws NoSuchAlgorithmException {
        return this.usuarioRepository.findByUserAndPassword(usuarioDto.getUser(), usuarioDto.getPassword()).orElse(null);
    }


    @Override
    public Usuario cambiarClave(UsuarioDto usuarioDto) throws NoSuchAlgorithmException {
        Usuario respuesta = this.usuarioRepository.findByUserAndCorreo(usuarioDto.getUser(), usuarioDto.getCorreo()).orElse(null);
        if (respuesta != null) {
            respuesta.generarPassword();
            usuarioDto.setPassword(respuesta.getPassword());
            usuarioDto.setNombre(respuesta.getNombre());
            usuarioDto.setId(respuesta.getId());
            usuarioDto.setApellido(respuesta.getApellido());
            usuarioDto.setRol(respuesta.getRol());
            actualizar(usuarioDto);
            return respuesta;
        } else {
            return null;
        }
    }

    @Override
    public String registrar(UsuarioDto usuarioDtoSave) {
        Usuario dtoUsuarioSave = new Usuario();
        dtoUsuarioSave.setId(usuarioDtoSave.getId());
        dtoUsuarioSave.setNombre(usuarioDtoSave.getNombre());
        dtoUsuarioSave.setApellido(usuarioDtoSave.getApellido());
        dtoUsuarioSave.setCorreo(usuarioDtoSave.getCorreo());
        dtoUsuarioSave.setUser(usuarioDtoSave.getUser());
        dtoUsuarioSave.setPassword(usuarioDtoSave.getPassword());
        dtoUsuarioSave.setRol(usuarioDtoSave.getRol());
        usuarioRepository.save(dtoUsuarioSave);
        return "Registro Exitoso";
    }

    @Override
    public String actualizar(UsuarioDto usuarioDtoUpdate) {
        Usuario dtoUsuarioUpdate = new Usuario();
        dtoUsuarioUpdate.setId(usuarioDtoUpdate.getId());
        dtoUsuarioUpdate.setNombre(usuarioDtoUpdate.getNombre());
        dtoUsuarioUpdate.setApellido(usuarioDtoUpdate.getApellido());
        dtoUsuarioUpdate.setCorreo(usuarioDtoUpdate.getCorreo());
        dtoUsuarioUpdate.setUser(usuarioDtoUpdate.getUser());
        dtoUsuarioUpdate.setPassword(usuarioDtoUpdate.getPassword());
        dtoUsuarioUpdate.setRol(usuarioDtoUpdate.getRol());
        usuarioRepository.save(dtoUsuarioUpdate);
        return "Registro Exitoso";
    }

    @Override
    public String eliminar(Long id) {
        usuarioRepository.deleteById(id);
        return "Eliminado con Exito";
    }

    @Override
    public Usuario consultarUsuarioById(Long id) {
        return usuarioRepository.findById(id).orElse(null);
    }

    @Override
    public List<Usuario> consultarUsuarios() {
        return (List<Usuario>) usuarioRepository.findAll();
    }
}
