package com.example.tourcali;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TourcaliApplication {

    public static void main(String[] args) {
        SpringApplication.run(TourcaliApplication.class, args);
    }

}
