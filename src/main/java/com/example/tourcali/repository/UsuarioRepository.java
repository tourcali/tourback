package com.example.tourcali.repository;

import com.example.tourcali.model.Usuario;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface UsuarioRepository extends CrudRepository<Usuario, Long> {

   Optional<Usuario> findByUserAndPassword(String user, String password);

   Optional<Usuario> findByUserAndCorreo(String user, String correo);
}