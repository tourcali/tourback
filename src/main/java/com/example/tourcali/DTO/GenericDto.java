package com.example.tourcali.DTO;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class GenericDto {

    private Integer status;
    private Object payload;

    public static GenericDto sucess(Object data) {

        GenericDto genericDto = new GenericDto();
        genericDto.status = (HttpStatus.OK.value());
        genericDto.payload = (data);

        return genericDto;
    }

}
